﻿# SSL/TLS 

Transport Layer Security (Sécurité de la couche transport) et SSL (Secure Sockets Layer) sont des protocoles de sécurisation des échanges sur internet. 

SSL à été développé par Netscape et l'IETF à poursuivi le dev de celui ci en le renommant TLS. 

SSL/TLS permet de créer un lien chiffré entre un serveur web et un navigateur. Cela permet de garantir que les données seront protégé contre la surveillance et la falsification(Man in the middle). 

Pour ce faire tout comme le protocole ssh il y'a un échange de clé. 

Voici un exemple de fonctionnement de négociation SSL standard avec un échange de clé RSA :

**1. Message « Client Hello »**

Informations que le serveur doit communiquer au client via SSL. Elles incluent le numéro de version SSL, les paramètres de chiffrement, les données spécifiques de la session.  

**4. Déchiffrement et secret maître**

Le serveur utilise sa clé privée pour déchiffrer le secret pré-maître. Le serveur et le client exécutent tous les deux les étapes de génération du secret maître avec le chiffrement convenu.

**2. Message « Server Hello »**

Informations que le serveur doit communiquer au client via SSL. Elles incluent le numéro de version SSL, les paramètres de chiffrement, les données spécifiques de la session.

**5. Chiffrement avec la clé de session** 

Le client et le serveur échangent des messages pour s'informer mutuellement que les futurs messages seront chiffrés.

**3. Authentification et secret pré-maître**

Le client authentifie le certificat de serveur (par exemple, Nom commun/Date/Émetteur). Le client (en fonction du chiffrement) crée le secret pré-maître pour la session, chiffre avec la clé publique du serveur et envoie le secret pré-maître chiffré au serveur.

Voici un schéma 
![](./images/SSL_handshake.jpg)
