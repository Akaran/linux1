# Apache httpd server

## Présentation

Apache http server est un serveur Web sous licence apache.
Il est disponible sur toutes les distributions Linux et unix ; très riche en fonctionnalités, modulable et extensible : 
Site Virtuels, Chiffrement SSL/TLS, Authentifications, Proxy, etc...

Ce service :

- est formé par une grappe de processus :
  - Un processus parent pour le contrôle
  - Des processus fils pour le traitement des requêtes
- Il est configuré à partir d'un unique fichier de configuration (/etc/httpd/httpd.conf) qui peu inclure d'autre fichier
- Il est modulaire, il peu donc intégrer des module offrant des fonctionalités supplémentaire
- Il fonctionne à partir d'une sous arborescence pour le serveur : Le ServerRoot (/etc/httpd)
- diposera pour chaque site hébergé, d'une sous arborescence dédiée : le DocumentRoot

### Fonctionnement sur du contenu 'static'

- Une requète est transmise et fourni un chemin au serveur (web-route/page)
- Le serveur httpd traduit ce chemin en un fichier sur l'arborescence de fichiers à partir du DocumentRoot du site interrogé.
- En fonction du paramétrage (des options) le fichier est alors transmi au client

## Configuration

l'unique fichier de configuration (/etc/httpd/httpd.conf) est en général divisé en sous fichiers via l'inclusion de ceux-ci avec la directive Include :

```
Include $file-specs
Include conf.d/   #tout les fichiers de ce répertoire seront inclus dans la config
```

la configuration est séparé en 3 parties :

- Gestion du Serveur et de son comportement
- Gestion du serveur web par défaut
- Gestion des autres serveur web, les virtualhost

### Les directives de configuration

Le fichier contiens un ensemble de directives et des de balises : `<balise> </balise>`.

__Chaque balise et chaque directive de configuration est rattaché à un des module du serveur__. Ce module devra alors être chargé (directive LoadModule) afin que la configuration soit lisible et interprétable par le server.

Les modules offrent des fonctionalités qui sont alors activées et/ou paramettrées par les directives associées. Le module core est le module principal

Pour chaque directive nous pouvons nous rèfèrer à la documentation officiel : https://httpd.apache.org/docs/2.4/fr/mod/quickreference.html dans la présentation de la directive on retrouvera alors sa description sa syntaxe dans quel contexte elle peu être utilisé et le module associé.

> Cette documentation est bien faite, vous devez vous y refèrer avant toute autre recherche.

__Directives principales :__

- `ServerRoot` défini le dossier racine du serveur
- `User` / `Group` defini l'identité avec laquel la grappe de processus s'exécutera sur le système
- `LoadModule` permet de charger des modules supplémentaire : `LoadModule alias_module /usr/lib/apache2/modules/mod_alias.so`
- `Listen` permet de définir un socket qui sera ouvert pour le serveur : Listen 127.0.0.1:80
- `DocumentRoot` : défini la racine d'un site web 
- `Alias` : permet définir une web-route : Alias /web-route/ /un/chemin/spécifique/sur/le/serveur

__balises principales :__

- la directive directory permùet de défin ir le coportement du swerveur web sur une partie de l'arborescence :

```xml
<Directory "/usr/share/apache2/icons">
    Options FollowSymlinks
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>
```

- La directive Files modifie le comportement du serveur pour une catégorie de fichier

```xml
<Files ~ "^\.ht">
    Order allow,deny
    Deny from all
    Satisfy all
</Files>
```

- La directive Location définie un comportement spécifique pour une URL donnée

```xml
<Location /server-info>
    SetHandler server-info
    Order deny,allow
    Deny from all
    Allow from 127.0.0.1 ::1
</Location>
```
vous noterez :

- Les directives `Options` et `AllowOverride` qui défini le comportement du serveur web
- La directive `SetHandler` qui précise comment répondre au requête (ici c'est un handler défini dans le module server-info)
- La gestion des accès via les directives :
  - `Order` défini l'ordre de validation des directive Deny et Allow : `Order deny,allow|allow,deny`
  - `Deny` défini dans quel cas refuser l'accés : `Deny from 10.0.2.0/24`
  - `Allow` defini dans quel cas authoriser l'accès : `Allow from lab.local`
  Ces directives son liées au module : mod_authz_host

__Les Options :__

- none : aucune option
- All : toutes options sauf MultiViews
- ExecCGI : permet l'exécution des CGI (les page sont générée par des programme exécuté par apache ! dangereux !)
- FollowSymLinks : permet le suivi des liens symboliques (attention les directives directory peuvent être alors inopérantes)
- Indexes : Permet l'indexation des répertoirs (on peu voir le contenu du dossier)
- MultiViews : gestion de contenue négocié avec le navigateur (notament suivant les langues)

### Exemple de configuration simple

```bash
ServerRoot "/tmp/httpd"
LockFile /tmp/httpd/accept.lock
PidFile /tmp/httpd/apache2.pid
User www-data
Group www-data
Timeout 300
KeepAlive On
MaxKeepAliveRequests 100
KeepAliveTimeout 15
<IfModule mpm_prefork_module>
    StartServers          5
    MinSpareServers       5
    MaxSpareServers      10
    MaxClients          150
    MaxRequestsPerChild   0
</IfModule>

DefaultType text/plain
HostnameLookups Off

LogLevel warn
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined

Listen 82

DocumentRoot /tmp/httpd/www/
ErrorLog /tmp/httpd/log/error.log
CustomLog /tmp/httpd/log/access.log vhost_combined
```

### Les modules MPM

MPM pour Multi Process Module, ce module viens gèrer la grappe de processus httpd qui traite les requêtes

#### prefork

Mode de fonctionnement historique de httpd en mode non multithreadé : un processus fils est utlisé par client

- `+` la sécurité et stabilité chaque traitement d'un client est isolé dans un processus fils
- `-` Les performances : augmentation du nombre de processus  et d'utilisation du cpu et de la ram

Ce mode reste courament utilisé car le module php n'est pas thread safe.

Configuration : On défini comment est géré la grappe de processus pré-forké

```xml
<IfModule mpm_prefork_module>
    StartServers          5
    MinSpareServers       5
    MaxSpareServers      20
    MaxClients          150
    MaxRequestsPerChild   0
</IfModule>
```

#### worker

Mode de fonctionnement privilégié du serveur httpd (apache 2)
Multithreadé, un thread par client.
- `+` partage de la mémoire et réduction de la consommation des ressources serveurs
- `+` utilisation des fork et des threads
- `-` Interdit l'usage de modules non thread safe

Mode de fontionnement devenu standard (hors usage de php)

Configuration : On défini comment est géré la grappe de processus et leur grappe de threads.

```xml
<IfModule mpm_worker_module>
    StartServers          2
    MinSpareThreads      25
    MaxSpareThreads      75 
    ThreadLimit          64
    ThreadsPerChild      25
    MaxClients          150
    MaxRequestsPerChild   0
</IfModule>
```

#### Autres Modules MPM

event : module worker avec gestions des connexions persistantes par un thread dédié. On évite alors les ouvertures fermeture de socket répétitives

mpm_`<system>` : module optimisé pour la gestion du multiprocessis sur le systeme `<system>` : winnt, os2, netware, beos.

### Configuration des site web hébergé

hormi le Site principale qui peut être défini dans la configuration principale du serveur web, les sites sont définis dans des balises __VirtualHost__ dépendant du module core.

Exemple :

```xml
<VirtualHost *:80>
        ServerAdmin webmaster@localhost 
        DocumentRoot /var/www
        ErrorLog /var/log/apache2/error.log
        CustomLog /var/log/apache2/access.log combined
</VirtualHost> 
```
On défini alors :

- sur quelle socket ce site web sera déservi : “*:80”, pour toutes les adresses IP écouté par httpd sur le port 80.
- le DocumentRoot du site
- le ServerAdmin : adresse mail jointe au message d'erreurs retourner qu client
- Les fichiers de log à utiliser pour ce site

Nous pouvons donc faire des VirtualHost par socket; httpd déssert des sites différents suivant le socket de connexion utilisé.

Cependant il est aussi possible de définir plusieurs site sur le même socket d'écoute, le site web sera déterminé en fonction du nom de serveur appelé.
Pour ce faire, on définiera un socket d'écoute sur lequel on activera la gestion des VirtualHost définis par leur noms :

```bash
NameVirtualHost adresse[:port]
```

Les VirtualHosts associées a ce socket d'écoute devront définir le nom de serveur dans la directive VirtualHost

```xml
<VirtualHost 1.2.3.4:80>
     ServerName toto.com
     ServerAlias www.toto.com
     …/...
</VirtualHost>
```

