# TP vagrant

## une solution wiki et une sauvegarde

Vous créez un vagrantfile qui démarre deux vm Debian wiki et backup avec une interface hostonly

Vous mettez en place une action de provisioning `init` : 

- installant une application dokuwiki sur les deux hosts
- installant rsync sur les deux hosts
- mettant en place une relation d'aprobation ssh de la vm dokuwiki vers la vm backup (authorized_keys)
- mettant en place une crontab root synchronisant le dossier data de la VM dokuwiki vers la vm backup toutes les 5 minutes.

Vous testerez une création de page et sa consultation sur la vm backup 5 minutes plus tard.
